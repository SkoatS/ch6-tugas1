
create table strikers (
	NAMA_ID varchar (30) not null primary key,
	umur numeric(4),
	klub varchar(10)
);

create table backs (
	TINGGI_ID varchar (10) not null primary key, 
	berat numeric(4),
	NAMA_ID varchar(30),
	
	constraint NAMA_ID foreign key (NAMA_ID) references strikers(NAMA_ID)
);


create table midfields (
	negara varchar(10),
	NAMA_ID varchar(30), 
	TINGGI_ID varchar (10),
	
	constraint NAMA_ID foreign key (NAMA_ID) references strikers(NAMA_ID),
	foreign key(TINGGI_ID) references backs(TINGGI_ID)
);

insert into strikers(NAMA_ID, umur, klub) values
('Lionel Messi', 35, 'PARIS SG');

insert into backs (TINGGI_ID, berat, NAMA_ID) values
(169, 67, 'Lionel Messi');

insert into midfields (negara, NAMA_ID, TINGGI_ID) values
('Argentina', 'Lionel Messi', 169);